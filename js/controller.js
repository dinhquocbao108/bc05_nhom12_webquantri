let turnOnLoading = () =>
	(document.getElementById('loading').style.display = 'flex');

let turnOffLoading = () =>
	(document.getElementById('loading').style.display = 'none');

let renderProductList = (data) => {
	let contentHTML = '';
	data.forEach((item) => {
		contentHTML += `
        <tr>
            <td class="text-center">${item.id}</td>
            <td>${item.name}</td>
            <td>${item.price}VND</td>
            <td><img src="${item.image}" alt=""></td>
            <td>${item.desc}</td>
            <td class="d-flex align-center">
            <button onclick="removeProduct(${item.id})">
            Xóa
            </button>
            <button onclick="editProduct(${item.id})"
            data-toggle="modal" data-target="#exampleModalCenter">Xem</button>
            </td>
        </tr>
        `;
	});
	document.getElementById('tr-content').innerHTML = contentHTML;
};

function layThongTinTuForm() {
	let nameProduct = document.getElementById('name__product').value;
	let priceProduct = document.getElementById('price__product').value;
	let imageProduct = document.getElementById('image__product').value;
	let descProduct = document.getElementById('desc__product').value;

	return {
		name: nameProduct,
		price: priceProduct,
		image: imageProduct,
		desc: descProduct,
	};
}

function disabled(id, display) {
	document.getElementById(id).style.display = display;
}
