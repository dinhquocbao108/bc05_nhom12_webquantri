function kiemTraRong() {
	let data = layThongTinTuForm();
	let isValied = true;
	switch ('') {
		case data.name:
			alert('Tên sản phẩm không được để trống!');
			isValied = false;
			break;
		case data.price:
			alert('Giá sản phẩm không được để trống!');
			isValied = false;
			break;
		case data.image:
			alert('Hình sản phẩm không được để trống!');
			isValied = false;
			break;
		case data.desc:
			alert('Phần mô tả sản phẩm không được để trống!');
			isValied = false;
			break;
	}
	return isValied;
}
function kiemTraGia() {
	let value = document.getElementById('price__product').value;
	let reg = /^\d+$/;
	let isNumber = reg.test(value);
	if (isNumber) {
		return true;
	} else {
		alert('Giá sản phẩm phải là số!(VND)');
		return false;
	}
}
